function Task(data) {
	this.description = ko.observable(data.description);
	this.complete = ko.observable(data.complete);
	this.created_at = ko.observable(data.created_at);
	this.updated_at = ko.observable(data.updated_at);
	this.id = ko.observable(data.id);
	this.isvisible = ko.observable(true);
}

function List(data) {
	this.name = ko.observable(data.name);
	this.id = ko.observable(data.id);
	this.isvisible = ko.observable(true);				
}

List.prototype.beginEdit = function(transaction) {
    	this.name.beginEdit(transaction);
	}

/*----------------------------------------------------------------------*/
/* Observable Extention for Editing
/*----------------------------------------------------------------------*/
ko.observable.fn.beginEdit = function (transaction) {
    
    var self = this;
    var commitSubscription, 
        rollbackSubscription;
    
    // get the current value and store it for editing
    if (self.slice)
        self.editValue = ko.observableArray(self.slice());
    else
        self.editValue = ko.observable(self());
        
    self.dispose = function () {        
        // kill this subscriptions
        commitSubscription.dispose();
        rollbackSubscription.dispose(); 
    };
    
    self.commit = function () {
        // update the actual value with the edit value
        self(self.editValue());
        
        // dispose the subscriptions
        self.dispose();
    };
        
    self.rollback = function () {
        // rollback the edit value
        self.editValue(self());
        
        // dispose the subscriptions
        self.dispose();
    };
    
    //  subscribe to the transation commit and reject calls
    commitSubscription = transaction.subscribe(self.commit,
                                                self,
                                                "commit");
    
    rollbackSubscription = transaction.subscribe(self.rollback,
                                                    self,
                                                    "rollback");
    
    return self;
}

/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/

function TaskViewModel() {
//function ListViewModel() {
	var self = this;
	self.lists = ko.observableArray([]);
	self.list.tasks = ko.observableArray([]);
	self.newListName = ko.observable();
	self.sortedBy = [];
	self.editingItem = ko.observable();
	
	  //  create the transaction for commit and reject
    self.editTransaction = new ko.subscribable();
        
    //  helpers
    self.isItemEditing = function(list) {
        return list == self.editingItem();
    };

	
	$.getJSON("/lists", function(raw) {
		var lists = $.map(raw, function(item) {
			return new List(item)
		});
		self.lists(lists);
	});
	
	
	self.displayButton = ko.observable(true),
	self.displayForm = ko.observable(false),
	self.showForm = function() {
		self.displayForm(true).displayButton(false);
	},
	self.hideForm = function() {
		self.displayForm(false).displayButton(true);
	}
	
	

	// Operations
	self.addList = function() {
		var newlist = new List({
			name : this.newListName()			
		});
		
		$.getJSON(function(data) {
			newlist.name(data.name);
			self.lists.push(newlist);
			//  begin editing the new item straight away
       		self.editList(newlist);       		 
			self.saveList(newlist);
		})
				
	};
	
	self.removeList = function(list) {
		list._method = "delete";
		self.lists.destroy(list);		
		self.saveList(list);
	};

	self.destroyList = function(list) {
		list._method = "delete";
		self.lists.destroy(list);
		self.saveList(list);
	}; 
	

	self.saveList = function(list) {
		var l = ko.toJS(list);
		$.ajax({
			url : "/lists",
			type : "POST",
			data : l
		}).done(function(data) {
			list.id(data.list.id);
		});
	}
	
	self.removeList = function (list) {
        if (self.editingItem() == null) {
            var answer = true; // confirm('Are you sure you want to delete this list? ' + list.name());
            if (answer) {
                self.lists.remove(list)
            }
        }
    };
    
    self.editList = function (list) {
        if (self.editingItem() == null) {
            // start the transaction
            list.beginEdit(self.editTransaction);
            
            // shows the edit fields
            self.editingItem(list);
        }
    };
    
    self.applyList = function (list) {
        //  commit the edit transaction
        self.editTransaction.notifySubscribers(null, "commit");
        
        //  hides the edit fields
        self.editingItem(null);
    };
    
    self.cancelEdit = function (list) {
        //  reject the edit transaction
        self.editTransaction.notifySubscribers(null, "rollback");
        
        //  hides the edit fields
        self.editingItem(null);
    };
//}

//function TaskViewModel() {
	var t = this;
	t.tasks = ko.observableArray([]);
	t.newTaskDesc = ko.observable();
	t.sortedBy = [];
	t.query = ko.observable('');
	t.MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	$.getJSON("/lists/1/tasks", function(raw) {
		var tasks = $.map(raw, function(item) {
			return new Task(item)
		});
		t.tasks(tasks);
	});

	t.incompleteTasks = ko.computed(function() {
		return ko.utils.arrayFilter(t.tasks(), function(task) {
			return (!task.complete() && task._method != "delete")
		});
	});
	t.completeTasks = ko.computed(function() {
		return ko.utils.arrayFilter(t.tasks(), function(task) {
			return (task.complete() && task._method != "delete")
		});
	});

	// Operations
	t.dateFormat = function(date) {
		if (!date) {
			return "refresh to see server date";
		}
		var d = new Date(date);
		return d.getHours() + ":" + d.getMinutes() + ", " + d.getDate() + " " + t.MONTHS[d.getMonth()] + ", " + d.getFullYear();
	}
	t.addTask = function() {
		var newtask = new Task({
			description : this.newTaskDesc()
		});
		$.getJSON("/getdate", function(data) {
			newtask.created_at(data.date);
			newtask.updated_at(data.date);
			t.tasks.push(newtask);
			t.saveTask(newtask);
			t.newTaskDesc("");
		})
	};
	t.search = function(task) {
		ko.utils.arrayForEach(t.tasks(), function(task) {
			if (task.description() && t.query() != "") {
				task.isvisible(task.description().toLowerCase().indexOf(t.query().toLowerCase()) >= 0);
			} else if (t.query() == "") {
				task.isvisible(true);
			} else {
				task.isvisible(false);
			}
		})
		return true;
	}
	t.sort = function(field) {
		if (t.sortedBy.length && t.sortedBy[0] == field && t.sortedBy[1] == 1) {
			t.sortedBy[1] = 0;
			t.tasks.sort(function(first, next) {
				if (!next[field].call()) {
					return 1;
				}
				return (next[field].call() < first[field].call()) ? 1 : (next[field].call() == first[field].call()) ? 0 : -1;
			});
		} else {
			t.sortedBy[0] = field;
			t.sortedBy[1] = 1;
			t.tasks.sort(function(first, next) {
				if (!first[field].call()) {
					return 1;
				}
				return (first[field].call() < next[field].call()) ? 1 : (first[field].call() == next[field].call()) ? 0 : -1;
			});
		}
	}
	t.markAsComplete = function(task) {
		if (task.complete() == true) {
			task.complete(true);
		} else {
			task.complete(false);
		}
		task._method = "put";
		t.saveTask(task);
		return true;
	}
	t.destroyTask = function(task) {
		task._method = "delete";
		t.tasks.destroy(task);
		t.saveTask(task);
	};
	
	t.removeAllComplete = function() {
		ko.utils.arrayForEach(t.tasks(), function(task) {
			if (task.complete()) {
				t.destroyTask(task);
			}
		});
	}
	
	t.saveTask = function(task) {
		var t = ko.toJS(task);
		$.ajax({
			url : "/lists/1/tasks",
			type : "POST",
			data : t
		}).done(function(data) {
			task.id(data.task.id);
		});
	}
}

viewModel = new TaskViewModel()
ko.applyBindings(viewModel)
//ko.applyBindings(TaskViewModel(), document.getElementById("lists-container"));
//ko.applyBindings(ListViewModel(), document.getElementById("lists-container"));

// Activate jQuery Validation
//$("container").validate({ submitHandler: viewModel.save });


