var appData = [{
	name : "Complete test task for RubyGarage",
	tasks : [{				
		complete : false,
		description : "Open this mock-up in Adobe FireWorks"
				
	}, {		
		complete : true,
		description : "Attentively check the file"		
	},
	
	{		
		complete : false,
		description : "Write CSS & HTML"		
	}
	]
},

{
	name : "For Home",
	tasks : [{	complete : true, description : "Buy a milk" }, 
			 { complete : false, description : "Call Mam"	},
			 { complete : false, description : "Clean the room"	}]
}

];

ko.bindingHandlers.enterkey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
       var allBindings = allBindingsAccessor();

            $(element).on('keypress', 'input, textarea, select', function (e) {
                var keyCode = e.which || e.keyCode;
                if (keyCode !== 13) {
                    return true;
                }

                var target = e.target;
                target.blur();

                allBindings.enterkey.call(viewModel, viewModel, target, element);

                return false;
            });
        }
};


function Task(data) {
	this.Editable = ko.observable(false);
    this.description = ko.observable(data.description);
    this.complete = ko.observable(data.complete);
}

function List(data) {
	this.Editable = ko.observable(false);
	this.name = ko.observable(data.name);
	this.tasks = ko.observableArray(data.tasks);
}

var ListsModel = function(lists) {
	var self = this;
				
	self.lists = ko.observableArray(ko.utils.arrayMap(lists, 
		function(list) {
		return {
			Editable : ko.observable(false),
			name : list.name,			
			tasks : ko.observableArray(list.tasks)
		};
	}));
	/*
	//Load lists from server
	$.getJSON("/lists", function(raw) {
		var lists = $.map(raw, function(item) {
			return new List(item)
		});
		self.lists(lists);
	});			
	*/
	
	self.current = ko.observable();			

	self.addList = function() {
		self.lists.push({
			name : "",		
			tasks : ko.observableArray()
		});
	};

	self.removeList = function(list) {
		self.lists.remove(list);
		
	};
	
	self.editList = function(list) {
        this.Editable(true);
    };
    
    // stop editing an item.  Remove the item, if it is now empty
    
		self.stopEditing = function (list) {
			list.Editable(false);

			if (!list.name().trim()) {
				self.removeList(list);
			}
		};

	self.addTask = function(list) {
		list.tasks.push({
			complete : "false",
			description : "",
			Editable : ko.observable(false),
		});
	};
	

	self.removeTask = function(task) {
		$.each(self.lists(), function() {
			this.tasks.remove(task)
		})
	};
	
	 self.saveLists = function() {
        $.ajax("/lists", {
            data: ko.toJSON({ lists: self.lists }),
            type: "post", contentType: "application/json",
            success: function(result) { alert(result) }
        });
   };	

	self.save = function() {
		self.lastSavedJson(JSON.stringify(ko.toJS(self.lists), null, 2));
		self.saveLists;
	};

	self.lastSavedJson = ko.observable("")
	
};

ko.applyBindings(new ListsModel(appData));



