require 'rubygems'
require 'sinatra'
require 'data_mapper'
require File.dirname(__FILE__) + '/models.rb'
require 'json'
require 'date'
require 'sinatra/twitter-bootstrap'

class ToDo < Sinatra::Base
  register Sinatra::Twitter::Bootstrap::Assets
  set :port, 9393
end


  before do
    content_type 'application/json'
  end

  get "/" do
    content_type 'html'
    
    erb :index
  end

  get "/lists/:list_id/tasks" do
    @list = List.get(params[:list_id])    
    @tasks = @list.tasks.all
    
    @tasks.to_json
  end    

  post "/lists/:list_id/tasks/new" do
    @list = List.get(params[:list_id])
    @task = @list.tasks.new(
    :complete => 'false',
    :description => params[:description],
    :created_at => DateTime.now,
    :updated_at => nil)
    #@list.save
    #@list.tasks << @task
    
    if @list.save
      {:task => @task, :status => "success"}.to_json
    else
      {:task => @task, :status => "failure"}.to_json
    end    
    
  end

  put "/lists/:list_id/tasks/:id" do
    #@list = List.find(params[:list_id])
    
    @list = List.get(params[:list_id])
    @task = @list.tasks.get(params[:id])
    @task.complete = params[:complete]
    @task.description = params[:description]
    @task.updated_at = DateTime.now
    if @task.save
      {:task => @task, :status => "success"}.to_json
    else
      {:task => @task, :status => "failure"}.to_json
    end
  end

  delete "/lists/:list_id/tasks/:id" do
    #@task = Task.find(params[:id])
    @list = List.get(params[:list_id])
    @task = @list.tasks.get(params[:id])
    if @task.destroy
      {:status => "success"}.to_json
    else
      {:status => "failure"}.to_json
    end
  end

  get "/getdate" do
    {:date => DateTime.now}.to_json
  end
  
  get "/lists" do
    @lists = List.all
    @lists.to_json
  end    

  post "/lists/new" do
    @list = List.new    
    @list.name = params[:name]       
    if @list.save
      {:list => @list, :status => "success"}.to_json      
    else
      {:list => @list, :status => "failure"}.to_json
    end
  end

  put "/lists/:list_id" do
    @list = List.get(params[:list_id])   
    @list.update(:name => params[:name])    
    if @list.save
      {:list => @list, :status => "success"}.to_json
    else
      {:list => @list, :status => "failure"}.to_json
    end
  end

  delete "/lists/:id" do
   @list = List.get(params[:id])   
    if @list.destroy 
      {:status => "success"}.to_json
    else
      {:status => "failure"}.to_json
    end
  end


