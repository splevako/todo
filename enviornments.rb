
configure :development, :test do
  DataMapper.setup(:default, "sqlite3://#{File.dirname(__FILE__)}/app.sqlite3") 
end

configure :production do
  DataMapper.setup(:default, ENV['DATABASE_URL'] || 'sqlite3://my.db')
end