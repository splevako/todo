require_relative 'spec_helper.rb'

describe 'Simple ToDo List app' do
  
  def app
    Sinatra::Application
  end

  it "says hello" do
    get '/'
    last_response.should be_ok    
  end
  
  it "add new ToDo list" do
    list = {:id=> 99, :name => "For Home"}
    post "/lists/new", list.to_json
    last_response.should be_ok      
  end
  
   it "show all ToDo lists" do
      list = List.create(:name => "List 7")
      list = List.create(:name => "List 8")     
      
      get "/lists"
      last_response.should be_ok       
   end
  
  it "edit todo list name " do
     list = List.create(:name => "For My Home")
           
     updated_list = {:name =>"For Work"}
     put "/lists/#{list.id}", updated_list.to_json     
     last_response.should be_ok
  end
  
   it "delete todo list" do
      list_to_delete = List.create(:name => "For My Home")      
      delete "/lists/#{list_to_delete.id}"
      last_response.should be_ok
   end
   
   it "add tasks to specific list" do
     list = List.create(:name => "Test List for ToDo app")
     
     task = {:description => "First task"}
     post "/lists/#{list.id}/tasks/new", task.to_json     
         
     last_response.should be_ok
   end
   
   it "get (show) all tasks" do
      list = List.create(:name => "List 6")
      
      list.tasks.create(:description => "Task 1") 
      list.tasks.create(:description => "Task 2") 
      list.tasks.create(:description => "Task 3") 
      
      get "/lists/#{list.id}/tasks"
      last_response.should be_ok       
   end
   
   it "update task - mark as complete and change task description" do
     list = List.create(:name => "Stub List")
     task = list.tasks.create(:description => "Buy a milk")     
          
     update_task = {:complete => true, :description => "Edit descr" }
     
     put "/lists/#{list.id}/tasks/#{task.id}", update_task.to_json
     
     last_response.should be_ok
   end
   
   it "delete task from specific project" do
     list = List.create(:name => "List 5")
     task = list.tasks.create(:id => 2, :description => "Work from home")         
     
     delete "/lists/#{list.id}/tasks/#{task.id}"
     
     last_response.should be_ok
   end
     
end