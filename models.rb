require "./enviornments.rb"
require 'dm-validations'

class Task
  include DataMapper::Resource
 
  property :id,                Serial ,  :key => true
  property :complete,          Boolean
  property :description,       Text    
  property :created_at,        DateTime
  property :updated_at,        DateTime
  
  #validates_length_of :description, :min => 1, 
  # :message => "Please provide the task description"
  #validates_presence_of :description
 
  belongs_to :list
end

class List
  include DataMapper::Resource
 
  property :id,          Serial,  :key => true 
  property :name,        Text
  
  #validates_length_of :name, :min => 1, 
  #:message => "Please enter the list name"
  
  #validates_presence_of :name
  
  has n, :tasks, constraint: :destroy
   
end

DataMapper::Model.raise_on_save_failure = true
DataMapper.finalize
DataMapper.auto_migrate!
List.auto_upgrade!
Task.auto_upgrade!
